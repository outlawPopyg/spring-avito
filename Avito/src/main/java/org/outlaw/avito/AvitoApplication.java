package org.outlaw.avito;

import org.outlaw.avito.models.Ad;
import org.outlaw.avito.models.PersonalItem;
import org.outlaw.avito.models.User;
import org.outlaw.avito.properties.RsaKeys;
import org.outlaw.avito.repositories.AdRepository;
import org.outlaw.avito.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.UUID;

@SpringBootApplication
@EnableConfigurationProperties(RsaKeys.class)
public class AvitoApplication {
//    @Bean
    @Transactional
    public CommandLineRunner commandLineRunner(UserRepository repository) {
        repository.save(User.builder()
                .name("Kalim").password("feif").email("fiejfi").build());
        return args -> {};
    }
    public static void main(String[] args) {
        SpringApplication.run(AvitoApplication.class, args);
    }

}
