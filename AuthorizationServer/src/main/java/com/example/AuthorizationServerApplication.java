package com.example;


import com.example.details.User;
import com.example.properties.RsaKeys;
import com.example.repositories.RoleRepository;
import com.example.repositories.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@SpringBootApplication
@EnableConfigurationProperties(RsaKeys.class)
public class AuthorizationServerApplication {

//    @Bean
//    @Transactional
//    public CommandLineRunner commandLineRunner(UserRepository userRepository, PasswordEncoder passwordEncoder) {
//        userRepository.save(
//                User.builder()
//                        .email("albert")
//                        .password(passwordEncoder.encode("outlaw"))
//                        .name("Albert")
//                        .build()
//        );
//        return args -> {};
//    }

    public static void main(String[] args) {
        SpringApplication.run(AuthorizationServerApplication.class, args);
    }
}
